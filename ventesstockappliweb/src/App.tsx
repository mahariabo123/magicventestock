import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Layout from "./layout";
import { Toaster } from "./components/ui/toaster";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout></Layout>,
  },
]);
function App() {
  return (
    <>
      <RouterProvider router={router}></RouterProvider>
      <Toaster></Toaster>
    </>
  );
}

export default App;
