import { Product } from "./product-service";
export type CartItem = {
  product: Product;
  count: number;
};
export default function addToCart(product: Product, count: number) {
  const cartItems = getCartItemsFromLocalStorage();
  console.log(cartItems);
  if (cartItems.length == 0) {
    let products: CartItem[] = [
      {
        product: product,
        count: count,
      },
    ];
    try {
      const cartString = JSON.stringify(products);
      localStorage.setItem("cart", cartString);
      return true;
    } catch (err) {
      console.log("Could not set the items to cart", err);
      return false;
    }
  }
  try {
    let isNewProduct: boolean = true;
    const updatedCart = cartItems.map((cartItem) => {
      if (cartItem.product.reference == product.reference) {
        isNewProduct = false;
        return { count: cartItem.count + count, product: cartItem.product };
      }
      return cartItem;
    });
    if (isNewProduct) {
      try {
        const cartString = JSON.stringify([{ product, count }, ...updatedCart]);
        localStorage.setItem("cart", cartString);
        return true;
      } catch (err) {
        console.log("Could not set the items to cart", err);
        return false;
      }
    }
    localStorage.setItem("cart", JSON.stringify(updatedCart));
    return true;
  } catch (err) {
    console.log("Could not parse the items from cart", err);
    return false;
  }
}
export function getCartItems() {
  return getCartItemsFromLocalStorage();
}

function getCartItemsFromLocalStorage(): CartItem[] {
  const cartJsonString = localStorage.getItem("cart");
  if (cartJsonString == null) return [];
  try {
    const products: CartItem[] = JSON.parse(cartJsonString);
    return products;
  } catch (err) {
    console.log("Could not parse the items from cart", err);
    return [];
  }
}
