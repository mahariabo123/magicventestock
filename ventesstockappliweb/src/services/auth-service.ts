import { Product } from "./product-service";
import { APIResponse } from "./response-type";

type Client = {
  numero: string | null;
  motDePasse: string | null;
  pseudo: string;
  nom: string;
  prenom: string;
};
export type LoginResponse = {
  jwt: string;
  client: Client;
  produit: Product[];
};
export async function login(
  username: string,
  password: string
): Promise<LoginResponse> {
  const response = await fetch(
    `${
      import.meta.env.VITE_APP_API_URL
    }/sessionPasserCde/auth/traiterIdentification`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json", // Spécifiez le type de contenu comme JSON
      },
      body: JSON.stringify({
        userName: username,
        password: password,
      }),
    }
  );
  let responseBody: APIResponse<LoginResponse>;
  try {
    responseBody = (await response.json()) as APIResponse<LoginResponse>;
  } catch (error) {
    console.error("Erreur lors de l'analyse de la réponse JSON :", error);
    throw new Error("Erreur lors du traitement de votre demande");
  }
  if (response.status != 200) {
    throw new Error(
      `Server responded with a non success code:  ${response.status}, ${responseBody.message}`
    );
  }
  return responseBody.data;
}
