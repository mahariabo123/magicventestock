import { useAuthStore } from "./store/auth-store";
import { PageAccueil } from "./vue/PageAccueil";
import { PageAccueilPerso } from "./vue/PageAccueilPerso";

export default function Layout() {
  const client = useAuthStore((state) => state.data?.client);
  if (client) {
    return <PageAccueilPerso></PageAccueilPerso>;
  }
  return <PageAccueil></PageAccueil>;
}
