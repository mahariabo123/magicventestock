import { LoginResponse } from "@/services/auth-service";
import { create } from "zustand";

interface AuthState {
  data: LoginResponse | undefined;
  login: (data: LoginResponse) => void;
}

const useAuthStore = create<AuthState>((set) => ({
  data: undefined,
  login: (data) => set({ data }),
}));

export { useAuthStore };
