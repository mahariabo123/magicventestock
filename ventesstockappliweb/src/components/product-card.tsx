import { type Product } from "@/services/product-service";
import MagicButton from "./magic-button";
import { Card, CardContent, CardFooter, CardHeader } from "./ui/card";
import { useState } from "react";
import { Button } from "./ui/button";
import { MinusIcon, PlusIcon } from "lucide-react";
import addToCart, { getCartItems } from "@/services/cart-service";
import { useToast } from "./ui/use-toast";

export default function ProductCard({ product }: { product: Product }) {
  const [productCount, setProductCount] = useState<number>(1);
  const { toast } = useToast();
  function handleSubstraction() {
    if (productCount > 1) {
      setProductCount((currentCount) => currentCount - 1);
    }
  }
  function handleAddition() {
    setProductCount((currentCount) => currentCount + 1);
  }
  function onAddToCart() {
    console.log("adding to cart");
    const success = addToCart(product, productCount);
    if (success) {
      toast({
        title: "Succès",
        description: "Le produit a été ajouté au pannier",
        variant: "default",
      });
      console.log(getCartItems());
    } else {
      console.log("error");
      toast({
        title: "Erreur",
        description: "Le produit n'a pas pu etre ajouté au panier",
        variant: "destructive",
      });
    }
  }
  return (
    <Card className="bg-gray-800 text-gray-50 border-none ">
      <CardHeader>
        <img
          alt="One Ring"
          className="aspect-square rounded-lg object-cover"
          height="300"
          src={product.image}
          width="300"
        />
      </CardHeader>
      <CardContent>
        <div className="grid gap-1">
          <h3 className="text-lg font-bold font-[Playfair Display, serif]">
            {product.libelle}
          </h3>
          <p className="text-sm text-gray-500 dark:text-gray-400 font-[Playfair Display, serif]">
            {product.descriptions}
          </p>
          <p className="text-sm text-gray-500 dark:text-gray-400 font-[Playfair Display, serif]">
            Quantité restant en stock: {product.quantitestock}
          </p>
        </div>
      </CardContent>
      <CardFooter className="w-full flex justify-between flex-col space-y-4">
        <div className="grid grid-cols-2">
          <p className="text-2xl font-bold font-[Playfair Display, serif]">
            $ {product.prix}
          </p>
          <div className="flex w-full gap-4">
            <div className="flex justify-center items-center w-1/6">
              <Button
                disabled={productCount == 1}
                variant={"ghost"}
                onClick={handleSubstraction}
              >
                <MinusIcon></MinusIcon>
              </Button>
            </div>
            <div className="flex items-center justify-center w-4/6 rounded-sm px-5 bg-gray-900 min-h-10 text-white">
              {productCount}
            </div>
            <div className="flex justify-center items-center w-1/6">
              <Button
                variant={"ghost"}
                onClick={handleAddition}
              >
                <PlusIcon></PlusIcon>
              </Button>
            </div>
          </div>
        </div>
        <MagicButton
          className="w-full"
          onClick={onAddToCart}
        >
          Ajouter au panier ✨
        </MagicButton>
      </CardFooter>
    </Card>
  );
}
