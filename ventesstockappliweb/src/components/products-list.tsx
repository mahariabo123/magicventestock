import { useAuthStore } from "@/store/auth-store";
import ProductCard from "./product-card";

export default function ProductsList() {
  const products = useAuthStore((state) => state.data?.produit);
  return (
    <section className="w-full py-12 md:py-24 lg:py-32 bg-gray-900 p-6 backdrop-blur-lg text-gray-50">
      <div className="container space-y-12 px-4 md:px-6">
        <div className="flex flex-col items-center justify-center space-y-4 text-center">
          <div className="space-y-2">
            <h2 className="text-3xl font-bold tracking-tighter sm:text-5xl font-[Playfair Display, serif]">
              Liste de nos produits
            </h2>
            <p className="max-w-[900px] text-gray-500 md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed dark:text-gray-400 font-[Playfair Display, serif]">
              Découvrez notre large gamme d'objets magiques, fabriqué à la main
              avec le plus grand soin et une attention aux détails.
            </p>
          </div>
        </div>
        <div className="mx-auto grid items-start gap-8 sm:max-w-4xl sm:grid-cols-2 md:gap-12 lg:max-w-5xl lg:grid-cols-3">
          {products?.map((product) => {
            return (
              <ProductCard
                product={product}
                key={product.id}
              ></ProductCard>
            );
          })}
        </div>
      </div>
    </section>
  );
}
