import { Button } from "@/components/ui/button";
import {
  DropdownMenuTrigger,
  DropdownMenuItem,
  DropdownMenuContent,
  DropdownMenu,
} from "@/components/ui/dropdown-menu";
import { LogOut, ShoppingCart, UserIcon } from "lucide-react";
import MagicButton from "./magic-button";
import React, { useState } from "react";
import { Sheet, SheetContent, SheetHeader, SheetTitle } from "./ui/sheet";
import Cart from "./cart";
import { useAuthStore } from "@/store/auth-store";

export default function Header({
  loginRef,
}: {
  loginRef: React.RefObject<HTMLDivElement>;
}) {
  const [cartOpen, setCartOpen] = useState<boolean>(false);
  const client = useAuthStore((state) => state.data?.client);
  function onLoginButtonClicked() {
    loginRef.current?.scrollIntoView({
      behavior: "smooth",
      block: "center",
    });
  }
  return (
    <>
      <Sheet
        open={cartOpen}
        onOpenChange={(isOpen) => setCartOpen(isOpen)}
      >
        <SheetContent className="w-1/2">
          <SheetHeader>
            <SheetTitle>Votre Panier</SheetTitle>
            <div className="mt-5">
              <Cart></Cart>
            </div>
          </SheetHeader>
        </SheetContent>
      </Sheet>

      <header className="px-4 lg:px-6 h-14 flex items-center bg-gray-900 text-gray-50">
        <div className="flex items-center gap-4">
          <h1 className="text-2xl font-bold">Magic Vente Stock</h1>
        </div>
        <div className="ml-auto flex items-center gap-4">
          {client ? (
            <DropdownMenu>
              <DropdownMenuTrigger asChild>
                <Button
                  className="text-gray-50"
                  variant="default"
                >
                  <div className="flex items-center space-x-3">
                    <div className="h-full bg-gray-500 rounded-full p-3">
                      <UserIcon></UserIcon>
                    </div>
                    <p>{client.pseudo}</p>
                  </div>
                </Button>
              </DropdownMenuTrigger>
              <DropdownMenuContent
                align="center"
                className=" flex flex-col items-center"
              >
                <DropdownMenuItem>
                  <Button
                    className="flex items-center gap-2"
                    variant="ghost"
                    onClick={() => setCartOpen(true)}
                  >
                    <ShoppingCart className="h-4 w-4" />
                    Mon panier
                  </Button>
                </DropdownMenuItem>
                <DropdownMenuItem>
                  <Button
                    className="flex items-center gap-2"
                    variant="ghost"
                  >
                    <LogOut className="h-4 w-4" />
                    Se deconnecter
                  </Button>
                </DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          ) : (
            <MagicButton onClick={onLoginButtonClicked}>
              Se connecter
            </MagicButton>
          )}
        </div>
      </header>
    </>
  );
}
