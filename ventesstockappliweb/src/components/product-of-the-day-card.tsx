import { Product } from "@/services/product-service";
import { MinusIcon, PlusIcon } from "lucide-react";
import MagicButton from "./magic-button";
import { Button } from "./ui/button";
import { useState } from "react";
import { useToast } from "./ui/use-toast";
import addToCart from "@/services/cart-service";
import { useAuthStore } from "@/store/auth-store";

export default function ProductOfTheDayCard({ product }: { product: Product }) {
  const [productCount, setProductCount] = useState<number>(1);
  const { toast } = useToast();
  const client = useAuthStore((state) => state.data?.client);
  function handleSubstraction() {
    if (productCount > 1) {
      setProductCount((currentCount) => currentCount - 1);
    }
  }
  function handleAddition() {
    setProductCount((currentCount) => currentCount + 1);
  }
  function onAddToCart() {
    if (client == null) {
      toast({
        title: "Authentification requise",
        description:
          "Veuillez vous authentifier ci-dessous pour ajouter l'article dans votre pannier",
        variant: "default",
      });
      return;
    }
    console.log("adding to cart");
    const success = addToCart(product, productCount);
    if (success) {
      toast({
        title: "Succès",
        description: "Le produit a été ajouté au pannier",
        variant: "default",
      });
    } else {
      console.log("error");
      toast({
        title: "Erreur",
        description: "Le produit n'a pas pu etre ajouté au panier",
        variant: "destructive",
      });
    }
  }
  return (
    <div className="  relative mx-4 w-full max-w-4xl rounded-2xl inset-0 bg-gradient-to-r from-yellow-400 via-red-500 to-pink-500 opacity-75 animate-pulse p-6 backdrop-blur-lg sm:p-8 grid grid-cols-2 gap-8">
      <div className="relative z-10">
        <div className="flex w-full h-full justify-center flex-col  space-y-4">
          <h3 className="font-bold tracking-tighter sm:text-3xl text-gray-900 font-[Playfair Display, serif]">
            {product.libelle}
          </h3>
          <p className="text-sm text-gray-600 dark:text-gray-400 font-[Playfair Display, serif]">
            {product.descriptions}
          </p>
          <p className="text-sm text-gray-600 dark:text-gray-400 font-[Playfair Display, serif]">
            Quantité restant en stock: {product.quantitestock}
          </p>
          <div className="grid grid-cols-2">
            <p className="text-2xl font-bold font-[Playfair Display, serif] text-gray-900">
              $ {product.prix}
            </p>
            <div className="flex w-full gap-4">
              <div className="flex justify-center items-center w-1/6">
                <Button
                  disabled={productCount == 1}
                  variant={"ghost"}
                  onClick={handleSubstraction}
                >
                  <MinusIcon></MinusIcon>
                </Button>
              </div>
              <div className="flex items-center justify-center w-4/6 rounded-sm px-5 bg-gray-900 min-h-10 text-white">
                {productCount}
              </div>
              <div className="flex justify-center items-center w-1/6">
                <Button
                  variant={"ghost"}
                  onClick={handleAddition}
                >
                  <PlusIcon></PlusIcon>
                </Button>
              </div>
            </div>
          </div>
          <MagicButton
            className="w-full"
            onClick={onAddToCart}
          >
            Ajouter au panier ✨
          </MagicButton>
        </div>
      </div>
      <div className="relative z-10 space-y-4">
        <img
          alt="One Ring"
          className="rounded-lg object-cover w-full"
          height="300"
          src={product.image}
          width="300"
        />
      </div>
    </div>
  );
}
