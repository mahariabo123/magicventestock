import { CartItem, getCartItems } from "@/services/cart-service";
import { useEffect, useState } from "react";

export default function Cart() {
  const [cartItems, setCartItems] = useState<CartItem[]>([]);
  useEffect(() => {
    const items = getCartItems();
    setCartItems(items);
  }, []);
  if (cartItems.length == 0) {
    return (
      <h3 className="text-gray-500">
        Vous n'avez pas encore d'article magiques dans votre panier
      </h3>
    );
  }
  return (
    <>
      {cartItems.map((cartItem) => {
        return (
          <div className="flex justify-between px-5 py-3 items-center">
            <p>{cartItem.product.libelle}</p>
            <p>{cartItem.count}</p>
          </div>
        );
      })}
    </>
  );
}
