import ProductsOfTheDay from "../components/products-of-the-day";
import Header from "../components/header";
import { useEffect, useRef } from "react";
import { useToast } from "@/components/ui/use-toast";
import { useAuthStore } from "@/store/auth-store";
import ProductsList from "@/components/products-list";

export function PageAccueilPerso() {
  const loginRef = useRef<HTMLDivElement>(null);
  const { toast } = useToast();
  const client = useAuthStore((state) => state.data?.client);
  useEffect(() => {
    if (client == null) return;
    toast({
      title: "Bienvenu en terre magique",
      description: `Bonjour ${client.nom} ${client.prenom}, nous vous souhaitons une très bonne visite dans notre boutique magique`,
    });
  }, []);
  return (
    <div className="flex flex-col min-h-[100dvh] font-[Playfair Display, serif]">
      <Header loginRef={loginRef}></Header>
      <main className="flex-1">
        <section className="w-full pt-12 md:pt-24 lg:pt-32 bg-gray-900 text-gray-50">
          <div className="container px-4 md:px-6 space-y-10 xl:space-y-16">
            <div className="grid max-w-[1300px] mx-auto gap-4 px-4 sm:px-6 md:px-10 md:grid-cols-2 md:gap-16">
              <div>
                <h1 className="lg:leading-tighter text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl xl:text-[3.4rem] 2xl:text-[3.75rem] font-[Playfair Display, serif]">
                  Découvrez les merveilles magiques de la Terre du Milieu
                </h1>
                <p className="mx-auto max-w-[700px] text-gray-300 md:text-xl dark:text-gray-400 font-[Playfair Display, serif]">
                  Explorez notre collection d'objets magiques authentiques
                  fabriqués à la main inspiré du monde du Seigneur des Anneaux.
                </p>
                <div className="space-x-4 mt-6"></div>
              </div>
              <img
                alt="Rivendell"
                className="mx-auto aspect-[21/9] overflow-hidden rounded-t-xl object-cover"
                height="600"
                src="/rivendell.png"
                width="1270"
              />
            </div>
          </div>
        </section>
        {/* Section produit du jour */}
        <ProductsOfTheDay></ProductsOfTheDay>
        {/* Section liste des produits*/}
        <ProductsList></ProductsList>
      </main>
    </div>
  );
}
