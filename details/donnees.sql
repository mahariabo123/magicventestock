INSERT INTO T_PRODUIT (ID, reference, libelle, estDuJour, prix, quantiteEnstock) VALUES
(CONCAT('Prod_', nextval('id_produit')), 'REF001', 'Anneau Unique', TRUE, 1000000.00, 1),
(CONCAT('Prod_', nextval('id_produit')), 'REF002', 'Epée d''Aragorn', FALSE, 1500.00, 10),
(CONCAT('Prod_', nextval('id_produit')), 'REF003', 'Arc de Legolas', FALSE, 1200.00, 5),
(CONCAT('Prod_', nextval('id_produit')), 'REF004', 'Hache de Gimli', FALSE, 900.00, 8),
(CONCAT('Prod_', nextval('id_produit')), 'REF005', 'Bâton de Gandalf', FALSE, 2000.00, 3),
(CONCAT('Prod_', nextval('id_produit')), 'REF006', 'Lembas', FALSE, 50.00, 100),
(CONCAT('Prod_', nextval('id_produit')), 'REF007', 'Cape d''invisibilité', FALSE, 750.00, 7),
(CONCAT('Prod_', nextval('id_produit')), 'REF008', 'Pierre de Vision de Galadriel', FALSE, 5000.00, 2),
(CONCAT('Prod_', nextval('id_produit')), 'REF009', 'Chevalier du Gondor', FALSE, 300.00, 20),
(CONCAT('Prod_', nextval('id_produit')), 'REF010', 'Armure d''Isildur', FALSE, 2500.00, 1),
(CONCAT('Prod_', nextval('id_produit')), 'REF011', 'Anneau des Nains', FALSE, 20000.00, 4),
(CONCAT('Prod_', nextval('id_produit')), 'REF012', 'Anneau des Elfes', FALSE, 30000.00, 3),
(CONCAT('Prod_', nextval('id_produit')), 'REF013', 'Anneau des Hommes', FALSE, 40000.00, 1),
(CONCAT('Prod_', nextval('id_produit')), 'REF014', 'Hallebarde du Rohan', FALSE, 1000.00, 6),
(CONCAT('Prod_', nextval('id_produit')), 'REF015', 'Heaume d''or du Rohan', FALSE, 800.00, 4),
(CONCAT('Prod_', nextval('id_produit')), 'REF016', 'Trompette de Gondor', FALSE, 600.00, 2),
(CONCAT('Prod_', nextval('id_produit')), 'REF017', 'Epée de Boromir', FALSE, 1400.00, 3),
(CONCAT('Prod_', nextval('id_produit')), 'REF018', 'Dague de Frodo', FALSE, 700.00, 5),
(CONCAT('Prod_', nextval('id_produit')), 'REF019', 'Miroir de Galadriel', FALSE, 4500.00, 1),
(CONCAT('Prod_', nextval('id_produit')), 'REF020', 'Cornemuse de Pippin', FALSE, 300.00, 10);

UPDATE T_PRODUIT SET image='anneau_unique.jpg',  description= 'L''Anneau Unique forgé par Sauron, possédant le pouvoir de contrôler tous les autres Anneaux de Pouvoir.' where reference = 'REF001';
UPDATE T_PRODUIT SET image='epee_aragorn.jpg',  description= 'L''épée légendaire portée par Aragorn, également connue sous le nom d''Andúril.' where reference = 'REF002';
UPDATE T_PRODUIT SET image='arc_legolas.jpg',  description= 'L''arc élégant et mortel utilisé par le prince elfe Legolas.' where reference = 'REF003';
UPDATE T_PRODUIT SET image='hache_gimli.jpg',  description= 'La hache robuste maniée par le nain Gimli, célèbre pour sa force et sa précision.' where reference = 'REF004';
UPDATE T_PRODUIT SET image='baton_gandalf.jpg',  description= 'Le bâton magique de Gandalf, utilisé pour canaliser ses pouvoirs mystiques.' where reference = 'REF005';
UPDATE T_PRODUIT SET image='lembas.jpg',  description= 'Le lembas, également connu sous le nom de pain de voyage elfique, nourrissant et délicieux.' where reference = 'REF006';
UPDATE T_PRODUIT SET image='cape_invisibilite.jpg',  description= 'La cape magique qui rend son porteur invisible.' where reference = 'REF007';
UPDATE T_PRODUIT SET image='pierre_galadriel.jpg',  description= 'La pierre mystique de Galadriel qui permet de voir des visions du futur ou du passé.' where reference = 'REF008';
UPDATE T_PRODUIT SET image='chevalier_gondor.jpg',  description= 'Les chevaliers courageux et nobles du royaume du Gondor.' where reference = 'REF009';
UPDATE T_PRODUIT SET image='armure_isildur.jpg',  description= 'L''armure légendaire portée par le roi Isildur lors de la bataille contre Sauron.' where reference = 'REF010';
UPDATE T_PRODUIT SET image = 'anneauxnains.png', description = 'L''un des sept Anneaux des Nains, forgés par le Seigneur des Ténèbres Sauron, ce puissant artefact est imprégné de magie ancienne et d''une sombre destinée. Conçu pour apporter richesse et prospérité à ses porteurs, cet anneau a néanmoins joué un rôle crucial dans les tragédies qui ont frappé les royaumes nains au cours des âges.' WHERE reference = 'REF011';
UPDATE T_PRODUIT SET image = 'anneauxelfes.jpg', description = 'Les Trois Anneaux des Elfes sont les plus puissants et les plus purs de tous les Anneaux de Pouvoir, créés par Celebrimbor et les artisans elfiques de l''Eregion sous la supervision indirecte de Sauron. Ces anneaux ont été conçus pour préserver, protéger et guérir, contrastant fortement avec les anneaux de pouvoir forgés pour dominer et corrompre. Chacun des Trois Anneaux des Elfes possède des propriétés uniques et est associé à l''un des trois éléments naturels.' WHERE reference = 'REF012';
UPDATE T_PRODUIT SET image = 'anneauxhommes.jpg', description = 'Les neuf Anneaux des Hommes, forgés par Sauron, accordent à leurs porteurs de grandes puissances et longévité, mais les corrompent progressivement. Ceux qui les portaient sont devenus les Nazgûl, les spectres serviteurs de Sauron, perdant leur humanité et leur libre arbitre.' WHERE reference = 'REF013';
UPDATE T_PRODUIT SET image = 'Hallebarde_Rohan.jpg', description = 'Une arme emblématique des guerriers du Rohan, connue pour sa lame acérée et sa portée redoutable, idéale pour la cavalerie et les combats en mêlée.' WHERE reference = 'REF014';
UPDATE T_PRODUIT SET image = 'Heaume_d_or.jpg', description = 'Un casque légendaire forgé en or pur, offrant une protection inégalée et symbolisant le courage et la royauté.' WHERE reference = 'REF015';
UPDATE T_PRODUIT SET image = 'Trompette_Gondor.jpg', description = 'Un instrument majestueux en argent, utilisé pour rallier les troupes et signaler l''arrivée de renforts, symbole de l''héroïsme et de la bravoure de Gondor.' WHERE reference = 'REF016';
UPDATE T_PRODUIT SET image = 'Epee-Boromir.jpg', description = 'Une épée majestueuse appartenant à Boromir, fils de Denethor, forgée avec une lame robuste et ornée de gravures, symbole de courage et de noblesse.' WHERE reference = 'REF017';
UPDATE T_PRODUIT SET image = 'Dague_de_frodo.jpg', description = 'Une dague ancienne et mystique, autrefois connue sous le nom de Dague des Nazgûl, récupérée par Frodo Sacquet lors de sa quête pour détruire l''Anneau Unique. Sa lame porte des inscriptions elfiques et elle émet une lueur faible en présence de ténèbres.' WHERE reference = 'REF018';
UPDATE T_PRODUIT SET image = 'Miroir_Galadriel.jpg', description = 'Un miroir ancien et magique appartenant à Galadriel, la Dame de Lórien. Ce miroir mystique permet de voir le passé, le présent et l''avenir, révélant des visions profondes et souvent prophétiques.' WHERE reference = 'REF019';
UPDATE T_PRODUIT SET image = 'Cornemuse_Pippin.jpg', description = 'Une cornemuse rustique et charmante appartenant à Peregrin Tuk, plus communément connu sous le nom de Pippin. Cet instrument simple mais joyeux accompagne souvent les festivités et les moments de camaraderie au sein de la Communauté de l''Anneau.' WHERE reference = 'REF020';


INSERT INTO T_CLIENT (numero, pseudo, motDePasse, nom, prenom) VALUES 
(CONCAT('cl_', nextval('id_client')), 'Elro', '$2a$10$aKSxXSuFjSFp8M1bkgVc4ewpcBA041./iov/mqNMVGD66oZCelBMW', 'ElroCaladhonhir', 'Caladhon'),
(CONCAT('cl_', nextval('id_client')), 'Thran', '$2a$10$Ed0tgCfyJGwbZMajZCvGxemL26yjPY.TMpwYaqEUSgviB1WTDCu42', 'Thranduil', 'Eärendil'),
(CONCAT('cl_', nextval('id_client')), 'Luth', '$2a$10$tm18Py9H3DXWZM4P8YKrVePtSxic1akeE85222v9IWjk2eQWlzoXm', 'Lúthien', 'Aranel');
