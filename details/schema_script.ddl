/*
Date de creation: 2024-06-08
*/

-- Table T_PRODUIT 

CREATE TABLE IF NOT EXISTS T_PRODUIT
(
    ID Varchar(255) primary key,
    reference Varchar(255),
    libelle Varchar(255),
    estDuJour Boolean,
    prix Float,
    quantiteEnstock int
);

CREATE SEQUENCE id_produit START WITH 1;

alter table T_PRODUIT add column image Varchar(255);
alter table T_PRODUIT add column description text;

CREATE TABLE IF NOT EXISTS T_CLIENT
(
    numero Varchar(255) PRIMARY KEY,
    pseudo Varchar(255),
    motDePasse Varchar(255),
    nom Varchar(255),
    prenom Varchar(255)
);

CREATE SEQUENCE id_client START WITH 1;