package com.example.backGondorChic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetierServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetierServicesApplication.class, args);
	}

}
