package com.example.backGondorChic.metier.metierServices;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.backGondorChic.metier.Modele.Client;

public class ClientsDetails implements UserDetails {
    private String numero;
    private String pseudo;
    private String password;
    private String nom;
    private String prenom;

    private List<GrantedAuthority> authorities;

    public ClientsDetails(Client userInfo) {
        pseudo = userInfo.getPseudo();
        password = userInfo.getMotDePasse();
        numero=userInfo.getNumero();
        nom=userInfo.getNom();
        prenom=userInfo.getPrenom();
        /*authorities = Arrays.stream(userInfo.getRoles().split(&quot;,&quot;))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());*/
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
            return authorities;
    }
    
    public String getNumero(){
        return this.numero;
    }

    public String getNom(){
        return this.nom;
    }

    public String getPrenom(){
        return this.prenom;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return pseudo;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
