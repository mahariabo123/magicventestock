package com.example.backGondorChic.metier.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.backGondorChic.metier.Modele.Produit;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit, String>  {
    List<Produit> findByEstDuJourTrue();
}