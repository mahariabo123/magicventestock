package com.example.backGondorChic.metier.metierServices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backGondorChic.metier.Modele.Produit;
import com.example.backGondorChic.metier.Repository.ClientRepository;
import com.example.backGondorChic.metier.Repository.ProduitRepository;

@Service
public class ProduitManager {

    @Autowired
    private ProduitRepository productRepository;

    public List<Produit> rechercherProduitDuJour() {
        return productRepository.findByEstDuJourTrue();
    }

    public List<Produit> getAllProduit(){
        return productRepository.findAll();
    }
}
