package com.example.backGondorChic.metier.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backGondorChic.metier.Modele.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client,String>{
    Optional<Client> findByPseudo(String pseudo);
}
