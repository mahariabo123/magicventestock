package com.example.backGondorChic.metier.metierServices;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.backGondorChic.metier.Modele.Client;
import com.example.backGondorChic.metier.Repository.ClientRepository;

@Service
public class ClientsManager implements UserDetailsService{
    /*public Client rechercheClientParPseudo(String pseudo,String motDePasse){
        
    }*/

    @Autowired
    private ClientRepository repository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Client> userDetail = repository.findByPseudo(username);

        // Converting userDetail to UserDetails
        return userDetail.map(ClientsDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found" + username));
    }

    public String addUser(Client userInfo) {
        userInfo.setMotDePasse(encoder.encode(userInfo.getMotDePasse()));
        repository.save(userInfo);
        return "User Added Successfully&quot";
    }
}
