package com.example.backGondorChic.metier.payload;

import java.util.List;

import com.example.backGondorChic.metier.Modele.Client;
import com.example.backGondorChic.metier.Modele.Produit;
import com.example.backGondorChic.metier.metierServices.ClientsDetails;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {
    //private Long id;
    private String jwt;
    private Client client;
    private List<Produit> produit;
    //private String pseudo;
    //private String nom;
    //private String prenom;
    

    
}
