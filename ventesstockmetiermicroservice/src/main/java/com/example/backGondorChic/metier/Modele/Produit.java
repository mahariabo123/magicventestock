package com.example.backGondorChic.metier.Modele;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;



@Entity
@Getter
@Setter
@Table(name = "t_produit")
@AllArgsConstructor
@NoArgsConstructor
public class Produit {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

   
    @Column(name = "reference",nullable = false)
    private String reference;

    @Column(name = "libelle",nullable = false)
    private String libelle;

    @Column(name="description",nullable=false)
    private String descriptions;

    @Column(name = "estdujour",nullable = false)
    private Boolean estDuJour;

    @Column(name = "prix",nullable = false)
    private BigDecimal prix;

    @Column(name = "quantiteenstock",nullable = false)
    private int quantiteEnStock;

    @Column(name="image",nullable=false)
    private String image;

    public Produit(String reference,String libelle,String descriptions,Boolean estDuJour,BigDecimal prix,int quantiteEnStock,String image){
        this.reference=reference;
        this.libelle=libelle;
        this.estDuJour=estDuJour;
        this.prix=prix;
        this.quantiteEnStock=quantiteEnStock;
        this.descriptions=descriptions;
        this.image=image;
    }

}
