package com.example.backGondorChic.metier.payload;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ApiResponse {
    public static ResponseEntity<Object> response(HttpStatus status, String message, Object data) {
        Map<String, Object> map = new HashMap<>();
        map.put("status", status.value());
        map.put("message", message);
        //System.err.println(data);
        if (data != null)
        {
            if (status == HttpStatus.BAD_REQUEST)
                map.put("errors", data);
            else
                map.put("data", data);
        }

        return new ResponseEntity<>(map, status);
    }
}
