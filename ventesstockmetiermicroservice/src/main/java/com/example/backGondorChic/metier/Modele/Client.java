package com.example.backGondorChic.metier.Modele;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="t_client")
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private String numero;

    @Column(name="pseudo",nullable = false)
    private String pseudo;
    @Column(name="motdepasse",nullable = false)
    private String motDePasse;
    @Column(name="nom",nullable = false)
    private String nom;
    @Column(name="prenom",nullable = false)
    private String prenom;

    public Client (String pseudo,String nom,String prenom){
        this.pseudo=pseudo;
        this.nom=nom;
        this.prenom=prenom;
    }


    /*public String getPseudo(){
        return this.pseudo;
    }

    public void setPseudo(String pseudo){
        this.pseudo=pseudo;
    }

    public String getNom(){
        return this.nom;
    }

    public void setNom(String nom){
        this.nom=nom;
    }

    public String getMotDePasse(){
        return this.motDePasse;
    }

    public void setMotDePasse(String motDePasse){
        this.motDePasse=motDePasse;
    }

    public String getPrenom(){
        return this.prenom;
    }

    public void setPrenom(String prenom){
        this.prenom=prenom;
    }*/


}
