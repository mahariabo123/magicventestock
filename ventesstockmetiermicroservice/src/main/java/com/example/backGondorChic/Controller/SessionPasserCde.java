package com.example.backGondorChic.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backGondorChic.metier.Modele.Client;
import com.example.backGondorChic.metier.Modele.Produit;
import com.example.backGondorChic.metier.Repository.ProduitRepository;
import com.example.backGondorChic.metier.metierServices.ClientsDetails;
import com.example.backGondorChic.metier.metierServices.ClientsManager;
import com.example.backGondorChic.metier.metierServices.JwtService;
import com.example.backGondorChic.metier.metierServices.ProduitManager;
import com.example.backGondorChic.metier.payload.ApiResponse;
import com.example.backGondorChic.metier.payload.AuthRequest;
import com.example.backGondorChic.metier.payload.JwtResponse;



@RestController
@RequestMapping("api/sessionPasserCde")
@CrossOrigin(origins = "*")
public class SessionPasserCde {
    
    @Autowired
    private ProduitManager produitManager;

    /*@Autowired
    private ClientsManager service;*/

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private ApiResponse apiResponse;

    private Client leClientIdentifie;

    private Produit leProduitCourant;
     @PostMapping("auth/traiterIdentification")
    public ResponseEntity<?> traiterIdentification(@RequestBody AuthRequest authRequest) {
        try{
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(),authRequest.getPassword()));
            String jwt=jwtService.generateToken(authRequest.getUserName());
            ClientsDetails clientDetails=(ClientsDetails)authentication.getPrincipal();
            leClientIdentifie=new Client(clientDetails.getUsername(),clientDetails.getNom(),clientDetails.getPrenom());
            List<Produit> produitduJour=produitManager.rechercherProduitDuJour();
            if(!produitduJour.isEmpty()){
                leProduitCourant=new Produit(produitduJour.get(0).getReference(),produitduJour.get(0).getLibelle(),produitduJour.get(0).getDescriptions(),produitduJour.get(0).getEstDuJour(),produitduJour.get(0).getPrix(),produitduJour.get(0).getQuantiteEnStock(),produitduJour.get(0).getImage());
            }
            
            return apiResponse.response(HttpStatus.OK,"OK", new JwtResponse(jwt,leClientIdentifie,produitManager.getAllProduit()));
           
        }
        catch(Exception e){
            return apiResponse.response(HttpStatus.BAD_REQUEST,"login ou mot de passe incorrecte", null);
           
        }

    }

    @GetMapping("/traiterAccesApplication")
    public List<Produit> traiterAccesApplication() {
            return produitManager.rechercherProduitDuJour();
    }


    
    //  @GetMapping("/produitCourant")
    // public List<Produit> getProduitCourant(){
    //     return produitManager.getAllProduit();
    // }

    /*@PostMapping("/addNewUser")
    public String addNewUser(@RequestBody Client userInfo) {
        return service.addUser(userInfo);
    }*/



   

}
